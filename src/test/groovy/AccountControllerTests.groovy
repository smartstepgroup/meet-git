import spock.lang.Specification

class AccountControllerTests extends Specification {

    def "should always pass"() {
        given:
        def min = 1;
        def max = 2;

        when:
        def result = Math.max(min, max)

        then:
        result == 2;
    }
}